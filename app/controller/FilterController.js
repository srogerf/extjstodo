Ext.define("Todo.controller.FilterController", {
  extend: "Ext.app.Controller",

  init: function () {
    var me = this;
    //this.
    //listen for filter clicks
    Ext.util.History.init('startHistory', this);
    if ("onhashchange" in window) { // event supported?
      window.onhashchange = function () {
        me.filterChange(window.location.hash);
      }
    } else { // event not supported:
      var storedHash = window.location.hash;
      window.setInterval(function () {
        if (window.location.hash != storedHash) {
          storedHash = window.location.hash;
          me.filterChange(storedHash);
        }
      }, 100);
    }
  },

  startHistory: function () {
    var me = this;
    Ext.util.History.mon({
      change: function () {
      },
      scope: me
    })
  },

  filterChange: function (evt) {
    console.log("FFFilt" + evt);
  }
});