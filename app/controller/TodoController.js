Ext.define("Todo.controller.TodoController", {
  extend: "Ext.app.Controller",

  init: function() {
    var me = this;
    this.countEl = Ext.getDom("todo-count");
    this.countHTML = this.countEl.innerHTML.replace("item", "TOKEN_ITEM").replace("1", "TOKEN_COUNT");
    this.doneCountEl = Ext.getDom("clear-completed");
    this.doneCountHTML = this.doneCountEl.innerHTML.replace("1", "TOKEN_COUNT");

    // listen to store changes
    this.store = Ext.data.StoreManager.lookup('TodoStore');
    this.store.load();

    this.store.on({
      datachanged: this.dataChanged,
      scope: me
    });

    //listen for new todos entered
    var todoEl = Ext.Element.get("new-todo");
    todoEl.on({
      change: this.create,
      scope: me
    });

    //clear clicks
    var clearAllEl = Ext.get("clear-completed");
    clearAllEl.on({
      click: this.deleteCleared,
      scope: me
    });

    //listen for toggle all
    var toggleAllEl = Ext.Element.get("toggle-all");
    toggleAllEl.on({
      click: this.clearAll,
      scope: me
    });

    //wait for the list container to be rendered
    this.control({
      "#list-container": {
        afterrender: function(container) {
          me.view = container;
          me.dataChanged();
        }
      }
    });
  },

  /**
   * create a new todo
   * @param evt
   */
  create: function(evt) {
    var text = evt.currentTarget.value;
    evt.currentTarget.value = "";
    var todo = Ext.create("Todo.model.TodoModel", { text: text });
    this.store.add(todo);
    //this.store.insert(0, todo);
    this.store.sync();
  },

  update: function(evt) {
    var toUp = this.store.getById(parseInt(evt.currentTarget.parentNode.firstChild.innerHTML));
    var checked = (evt.currentTarget.checked === true);
    toUp.set("done", checked);
    this.store.sync();
  },

  clearAll: function(evt) {
    var checked = (evt.currentTarget.checked === true);
    this.store.each(function(todo) { todo.set("done", checked) });
    this.store.sync();
  },

  deleteCleared: function() {
    var me = this;
    var cleared = this.store.query("done", "true");
    cleared.each(function(rec) { me.store.remove(rec); });
    this.store.sync();
  },

  remove: function(evt) {
    var toRemove = this.store.getById(parseInt(evt.currentTarget.parentNode.firstChild.innerHTML));
    this.store.remove(toRemove);
    this.store.sync();
  },

  show: function() {
    var me = this;
    this.view.update(this.store);
    var checks = Ext.select("input[class=toggle]");
    checks.on({
      click: this.update,
      scope: me
    });
    var deletes = Ext.select("button[class=destroy]");
    deletes.on({
      click: this.remove,
      scope: me
    });
  },

  setCounts: function() {
    var done = this.store.query("done", "true").length;
    var notDone = this.store.getCount() - done;
    //var c = this.store.query("done", "false").length;
    this.doneCountEl.innerHTML = this.doneCountHTML.replace("TOKEN_COUNT", done);
    this.countEl.innerHTML = this.countHTML.replace("TOKEN_COUNT", notDone).replace("TOKEN_ITEM", "item" + (notDone == 1 ? '' : 's'));
    Ext.getDom("toggle-all").checked = (notDone == 0 ? "checked" : "");

  },

  dataChanged: function() {
    this.setCounts();
    this.show();
  }



});