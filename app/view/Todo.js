Ext.define("Todo.view.Todo", {
  extend: 'Ext.Container',

  initComponent: function () {
    var me = this;

    var tpl = new Ext.XTemplate(
        '<ul id="todo-list">'
            + '<tpl for="."'
            + '<div class="view">'
            + '<li>'
            + "<div hidden>{data.id}</div>"
            + '<input class="toggle" type="checkbox" {[values.data.done == "true" ? "checked" : "" ]}>'
            + '<label>{data.text}</label>'
            + '<button class="destroy"></button>'
            + '</li>'
            + '</div>'
            + '</tpl>'
            + '</ul> '
    );

    Ext.applyIf(me, {
      items: [
        {
          // title area
          id: 'list-container',
          store: 'TodoStore',
          width: 548,
          tpl: tpl
        }
      ]
    });
    me.callParent(arguments);
  }
});