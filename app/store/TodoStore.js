Ext.define("Todo.store.TodoStore", {
  extend: "Ext.data.Store",

  requires: [
      "Todo.model.TodoModel"
  ],

  constructor: function(cfg) {
    var me = this;
    cfg = cfg || {};
    me.callParent([Ext.apply({
      autoLoad: true,
      storeId: 'TodoStore',
      model: 'Todo.model.TodoModel',
      proxy: {
        type: 'localstorage',
        id: 'todo-store'
      }
    }, cfg)]);
  }
});