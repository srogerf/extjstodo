/**
 * model representing a todo item
 */
Ext.define("Todo.model.TodoModel", {
  extend: "Ext.data.Model",

  fields: [
    { name: "text", type: "string" },
    { name: "done", type: "string" }
  ]
});